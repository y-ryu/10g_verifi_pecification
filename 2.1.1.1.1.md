#### 項目番号：2.1.1.1.1

#### 項目名：起動時シーケンス確認 –ネイティブ-

#### 仕様番号：

---
#### 検証構成番号：検証構成Lab.13

#### Radius attribute：HIKARI+MGCP+Native+4over6(v4TFTP)

---
##### 試験設定
+ [試験設定①](C:\WorkData\010_HDT3\015_NTT光コラボ10G 技術検討\検証手順書\setting files\試験設定①Web-UIの設定.md)  [試験設定②](C:\WorkData\010_HDT3\015_NTT光コラボ10G 技術検討\検証手順書\setting files\試験設定②その他設定.md)  [試験設定⑤](C:\WorkData\010_HDT3\015_NTT光コラボ10G 技術検討\検証手順書\setting files\試験設定⑤4over6フィルタ設定.md)

##### 試験パターン

+ 

---

#### **検証手順**

| 番号 | 手順 | 確認内容 | 参考資料 |
| :--- | :--- | :--- | :--- |
| | **前手順** | | |
| | **試験手順** | | |
| 1 | 試験『10.3.1.1.1 モデム起動後のWANネットワーク動作』で取得したLOGを確認 | | |
| 2 | 上記試験手順4,11のキャプチャログおよびコマンドログを確認 | 試験端末からIPv6 Radiusサーバに向けてRadiusの『Access Request』が送信されていること ||
||| 試験端末からIPv4 Radiusサーバに向けてRadiusの『Access Request』が送信されていないこと ||
||| IPv6 Radiusサーバから試験端末に向けてRadiusの『Access Accept』が送信されていること ||
||| 『radclient showstatistics』コマンドにて、Access-RequestおよびAccess-Acceptの値がカウントアップされていること ※補足2 | [CLI情報](C:\WorkData\010_HDT3\015_NTT光コラボ10G 技術検討\検証手順書\CLI info.md) |
||| 『radclient showreceived』コマンドにて、RadiusServerに設定した値を全て受信していること ※補足2 | [CLI情報](C:\WorkData\010_HDT3\015_NTT光コラボ10G 技術検討\検証手順書\CLI info.md) |
||| 『radclient showsettings』コマンドにて、各Attributeの値が正しく試験端末に設定されていること ※補足2 | [CLI情報](C:\WorkData\010_HDT3\015_NTT光コラボ10G 技術検討\検証手順書\CLI info.md) |
||| 試験端末起動後にWAN I/Fを接続した場合にも、正常にRadius通信が実行されること (判定基準2～7の内容を確認) ||
| |  **後手順** | | |