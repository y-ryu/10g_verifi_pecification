#### 項目番号：2.1.1.2.4

#### 項目名：Reject受信時 (更新時)　 -SIPv6・ネイティブ-

#### 仕様番号：

---
#### 検証構成番号：検証構成Lab.13

#### Radius attribute：Native+4over6(v4TFTP)

---
##### 試験設定

+ 

##### 試験パターン

+ 

---

#### **試験手順**

| 番号 | 手順 | 確認内容 | 参考資料 |
| :--- | :--- | :--- | :--- |
| | **前手順** |||
| 1 | ~~コンソールPCにて、コマンドログを取得開始~~ | ~~コマンドログ:項目番号_con_yymmdd.log~~ | |
| 2 | ~~事前事後確認シェルを実施~~ | ~~試験ファームウェアであること~~ | |
|  |  | Syslogにて異常なメッセージが出力されていないこと | |
| 3 | キャプチャPC①にてキャプチャ開始 |  | |
| | **試験手順** |||
| 1 | 試験端末起動後、Radius Attributeを取得 |                                                              |          |
| 2    | 試験パターンごとにWMTA Radiusサーバの設定を変更(2.1.1.2.4_m.xlsを参照) |                                                              |          |
| 3    | 『radclient restart』コマンドで、試験端末にてRadius Attributeを再取得 |                                                              |          |
| 4    | Access Rejectを受信後、『radclient showstatistics』コマンドにて試験端末のRadius統計情報を確認 | Access Rejectを受信時をRadius統計情報にてカウントアップされていることを確認 |          |
| 5    | 『radclient showsettings』コマンドにて試験端末のRadius設定情報を確認 | Access Reject受信後、前回の設定が維持されていることを確認    |          |
| 6    | 『syslog show』コマンドにて試験端末のSyslogを確認            | 再起動後、 コマンドのログに下記ログが保存されていること<br/>TIME : 機種　 Radius attribute accept fail |          |
| 7    | Syslogサーバのログを確認                                     | Syslogサーバの『Message』に下記ログが保存されていること <br/>TIME IP : 機種　 Radius attribute accept fail <br/>Syslogサーバのログの『Msg Type』が『syslog.warning』(Facility：14、Severity：4)であること |          |
| 8    | キャプチャPC①にてキャプチャを確認                            | Access RejectのMessage-Replyが試験パターンの『Reply-Message』の値になっていること |          |
| 9    |                                                              | インターネット回線LEDが『橙点灯』、BBフォンLEDが『緑点灯』であること |          |
| 10   | SIPv6(0ABJ)発着信を実施                                      | SIPv6での発着信が可能なこと                                  |          |
| 11   | Radius Serverの設定を正常に戻す                              | Access Reject受信後、試験パターンの『AccessRequest再送時間』後にAccessRequestを送信すること |          |
| 12   | AccessRequest送信後（AccessAccept受信後）のLED状態を確認     | インターネット回線LEDが『緑点灯』、BBフォンLEDが『緑点灯』であること |          |
| 13   | SIPv6(0ABJ)発着信を実施                                      | SIPv6での発着信が可能なこと                                  |          |
| | **後手順** | | |
| 1 | ~~事前事後確認シェルを実施~~ | ~~試験ファームウェアであること~~ | |
| 2 |  | Syslogにて異常なメッセージが出力されていないこと | |
| 3 | ~~コンソールPCのコマンドログを停止~~ | ~~コマンドログは試験結果とともに共有フォルダへ保存すること~~ | |
| 4 | キャプチャPC①にてキャプチャ停止 | キャプチャログは試験結果とともに共有フォルダに保存すること<br/>キャプチャログ：項目番号_試験パターンNo_yymmdd.cap | |

